import './map.component.scss';

import { AbstractPlace } from '@/services/abstract-place';
import { Component, Prop, Vue } from 'vue-property-decorator';
import maps, { mapElement, TaxiDirection } from '@/services/maps';



@Component({
  template: `
    <div id="map-component">
      <div class="cheap-or-fast">
        <button v-for="route of currentDirections" :class="{ active: route.isSelected }" @click="changeRoute(route)" class="direction-type-button">
          <h4 class="direction-type-button__title" v-if="currentDirections.length > 1">{{ route.label }}</h4>
          <p class="direction-type-button__cost">{{ Math.ceil(route.getCost()*100)/100 }} BYN</p>
          <p class="direction-type-button__secondary-text">{{ route.distance.toFixed(1) }} km, {{ route.duration.toFixed() }} min</p>
        </button>
      </div>
      <div class="control-container">
        <button class="control-btn" @click="onCurrentLocationClick()">
          <icon class="location-arrow" name="location-arrow"></icon>
        </button>
      </div>
      <!-- mapElement will be appended here when mounted-->
    </div>
  `,
})

export default class Map extends Vue {
  
  @Prop() currentDirections!: TaxiDirection[];

  @Prop() departure!: AbstractPlace;

  @Prop() destination!: AbstractPlace;

  mounted() {
    this.$el.appendChild(mapElement);
  };

  changeRoute(direction: TaxiDirection) {

    this.currentDirections.forEach(r => r.isSelected = false);
    direction.isSelected = true;

    maps.selectRoute(direction);
  };

  onCurrentLocationClick() {
    maps.setCenterToCurrentLocation();
  }

};