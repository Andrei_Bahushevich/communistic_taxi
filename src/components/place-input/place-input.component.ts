import './place-input.component.scss';
import 'vue-awesome/icons/location-arrow';

import { AbstractPlace } from '@/services/abstract-place';
import { Component, Emit, Prop, Vue } from 'vue-property-decorator';
import maps, { PredictionPlace } from '@/services/maps';
import Debounce from '@/decorators/debounce.decorator';



@Component({
  template: `
    <div class="place-input" >
      <div class="search-box">
        <input class="location-input" :placeholder="placeholder" @focus="focusSearchBox" @blur="blurSearchBox" @keyup="searchPlaces(inputVal)" v-model="inputVal">
        <button class="current-location-button" @click="currentLocation"><icon class="location-arrow" name="location-arrow"></icon></button>
      </div>
      <div class="tip-list">
        <div class="tip-item" v-for="place in places" @pointerdown="chooseTip(place)">
          <h4 class="tip-item__main-text">{{ place.mainText }}</h4>
          <p class="tip-item__secondary-text">{{ place.secondaryText }}</p>
        </div>
      </div>
    </div>
    `
})
export default class PlaceInput extends Vue {
  selectedPlace: AbstractPlace = new PredictionPlace();
  inputVal = '';
  places: PredictionPlace[] = [];

  @Prop(String) placeholder: any;

  @Prop(String) placeInputType: any;

  @Emit('place-selected')
  emitPlaceSelected(place: AbstractPlace) { };

  mounted() {
    if (this.placeInputType === "departure") {
      this.currentLocation();
    };
  };

  @Debounce(400)
  searchPlaces(val: string) {
    if (val) {
      maps.getPlaces(val).then(predictionPlace => {
        this.places = predictionPlace;
      });
    } else {
      this.places = [];
    };
  };

  focusSearchBox() {
    if (this.selectedPlace.getViewText() === 'Current location') {
      this.inputVal = '';
    };
  };

  blurSearchBox() {
    this.inputVal = this.selectedPlace.getViewText();
    this.places = [];
  };

  currentLocation() {
    maps.getCurrentLocation().then(location => {
      this.selectedPlace = location;
      this.blurSearchBox();
      this.emitPlaceSelected(location);
    }).catch((err) => {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    });
  };

  chooseTip(place: PredictionPlace) {
    this.selectedPlace = place;
    this.emitPlaceSelected(place);
    this.blurSearchBox();
  };
};