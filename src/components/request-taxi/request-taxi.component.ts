import './request-taxi.component.scss';

import { AbstractPlace } from '@/services/abstract-place';
import { Component, Vue } from 'vue-property-decorator';
import maps, { PredictionPlace, TaxiDirection } from '@/services/maps';
import PlaceInput from '../place-input/place-input.component'
import MapComponent from "../map/map.component";

@Component({
  template: `
    <div class="request-taxi-component">
      <MapComponent
        :currentDirections="currentDirections"
        :departure="departure"
        :destination="destination"
      />
      <div class="addresses">
        <PlaceInput 
            placeInputType="departure"
            placeholder="Departure address"
            @place-selected="placeSelected($event, 'departure')"
        />
        <hr>
        <PlaceInput 
            placeInputType="destination"
            placeholder="Destination address"
            @place-selected="placeSelected($event,'destination')"
            />
          </div>
        </div>
        `,
  components: { PlaceInput, MapComponent }
})

export default class RequestTaxi extends Vue {
  departure: AbstractPlace = new PredictionPlace();
  destination: AbstractPlace = new PredictionPlace();
  currentDirections: TaxiDirection[] = [];

  mounted() {
    maps.setCenterToCurrentLocation();
  }

  destroyed() {
    maps.clearMapDirections();
  }

  placeSelected(place: AbstractPlace, pointType: string) {
    if (pointType === 'departure') {
      this.departure = place;
    } else if (pointType === 'destination') {
      this.destination = place;
    };

    if (this.departure.mainText && this.destination.mainText) {
      maps.getRoutes(this.departure, this.destination).then(routes => {
        maps.displayRoutesOnMap(routes);
        this.currentDirections = routes;
      });
    };

    console.log('departure: ', this.departure);
    console.log('destination: ', this.destination);
  };
}; 