import './registerServiceWorker';

import Vue from 'vue';
import Icon from 'vue-awesome/components/Icon.vue';
import VueRx from 'vue-rx';

import AppComponent from './app.component';
import router from './router';


Vue.use(VueRx)

Vue.component('icon', Icon)

new Vue({
  router,
  render: h => h(AppComponent)
}).$mount('#app')
