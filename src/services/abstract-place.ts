export interface AbstractPlace {
  mainText: string;
  getViewText(): string;
  getPosition(): string | google.maps.LatLng;
};