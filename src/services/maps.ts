import { AbstractPlace } from '@/services/abstract-place';
import { Observable } from 'rxjs';
import { shareReplay, take, tap } from 'rxjs/operators';

const mapsScript = document.createElement('script');


mapsScript.setAttribute('src', `https://maps.googleapis.com/maps/api/js?key=AIzaSyAgaiEVw5J8x3HpKFbUGQVUjVRXqQYoTQw&callback=initMap&libraries=places`);

document.body.appendChild(mapsScript);

const $mapInit = new Promise((resolve, reject) => {
  (<any>window).initMap = () => {
    resolve((<any>window).google);
  }
});

export const $placesSrv: Promise<google.maps.places.PlacesService> = $mapInit.then(() => {
  return new google.maps.places.PlacesService(document.createElement('div')); //
});

export const mapElement = document.createElement('div');

export const $map = $mapInit.then(() => {

  mapElement.setAttribute('id', 'map');
  const mapOptions: google.maps.MapOptions = {
    zoom: 14,
    disableDefaultUI: true,
  };
  return new google.maps.Map(mapElement, mapOptions);
});

export const $directionsSrv = $mapInit.then(() => {
  return new google.maps.DirectionsService();
});

export const $directionsRendererSrv = $map.then((map) => {
  const srv = new google.maps.DirectionsRenderer();
  srv.setMap(map);
  return srv;
});

export const $autocompleteSrv = $mapInit.then(() => {
  return new google.maps.places.AutocompleteService();
});

export const $distanceMatrixSrv = $mapInit.then(() => {
  return new google.maps.DistanceMatrixService();
});

export default {
  async clearMapDirections() {
    const renderer = await $directionsRendererSrv;

    renderer.setDirections({
      geocoded_waypoints: [],
      routes: [],
    })
  },

  async getPlaces(input: string) {
    const autocompleteSrv = await $autocompleteSrv;
    const position = await this.getCurrentLocation();

    return new Promise<PredictionPlace[]>((resolve, reject) => {
      const requestOptions: google.maps.places.AutocompletionRequest = {
        input,
        location: position.getPosition(),
        radius: 100,
      };

      autocompleteSrv.getPlacePredictions(requestOptions, (predictionList: google.maps.places.AutocompletePrediction[]) => {
        if (predictionList) {

          const result: PredictionPlace[] = predictionList.map(prediction => {
            return new PredictionPlace(
              prediction.structured_formatting.main_text,
              prediction.structured_formatting.secondary_text,
            );
          });

          resolve(result);
        } else {
          resolve([]);
        };
      });
    });
  },

  async getRoutes(from: AbstractPlace, to: AbstractPlace) {
    return $directionsSrv.then((srv) => {
      return new Promise<TaxiDirection[]>((resolve, reject) => {

        const directionsRequest: google.maps.DirectionsRequest = {
          origin: from.getPosition(),
          destination: to.getPosition(),
          travelMode: google.maps.TravelMode.DRIVING,
          provideRouteAlternatives: true,
        };

        srv.route(directionsRequest, (directionsResult: google.maps.DirectionsResult) => {
          if (directionsResult) {
            const newTaxiDirections: TaxiDirection[] = directionsResult.routes.map((result, index) => {
              return new TaxiDirection(directionsResult, result, index);
            });

            var cheap: TaxiDirection = newTaxiDirections[0];
            var fast: TaxiDirection = newTaxiDirections[0];

            newTaxiDirections.forEach(item => {
              if (item.duration < fast.duration) {
                fast = item;
              };
              if (item.getCost() < cheap.getCost()) {
                cheap = item;
              };
            });

            cheap.isSelected = true;
            cheap.label = 'Cheap';
            const result = [cheap];
            if (cheap != fast) {
              fast.label = 'Fast';
              result.push(fast);
            }

            resolve(result);

          } else {
            reject();
          }
        });
      });
    });
  },

  async selectRoute(taxiDirection: TaxiDirection) {
    return $directionsRendererSrv.then(srv => {
      return srv.setRouteIndex(taxiDirection.routeIndex);
    });
  },

  async displayRoutesOnMap(directions: TaxiDirection[]) {
    return $directionsRendererSrv.then(srv => {
      srv.setDirections(directions[0].nativeDirections);

      const selectedRoute = directions.find(d => d.isSelected);
      if (selectedRoute) {
        srv.setRouteIndex(selectedRoute.routeIndex);
      }

    });
  },

  location: new Observable<Location>((observer) => {
    const watcher = navigator.geolocation.watchPosition(
      pos => observer.next(Location.fromPosition(pos)),
      err => observer.error(err),
    );

    return () => {
      navigator.geolocation.clearWatch(watcher);
    }
  }).pipe(shareReplay(1)),

  async getCurrentLocation() {
    return this.location.pipe(take(1)).toPromise();
  },

  async setCenterToCurrentLocation() {
    const position = await this.getCurrentLocation();
    const map = await $map;

    map.setCenter(position.getPosition());
  },

};

export class TaxiDirection {
  private perMin = 0.15;
  private perKm = 0.35;
  private reservation = 0;

  isSelected = false;
  distance = 0;
  duration = 0;

  label = '';

  constructor(
    public nativeDirections: google.maps.DirectionsResult,
    route: google.maps.DirectionsRoute,
    public routeIndex = -1,
  ) {
    this.distance = route.legs.reduce((elem, current) => {
      return elem + current.distance.value;
    }, this.distance) / 1000;

    this.duration = route.legs.reduce((elem, current) => {
      return elem + current.duration.value;
    }, this.duration) / 60;

  };
  getCost() {
    const cost = this.reservation
      + this.duration * this.perMin
      + this.distance * this.perKm;
    return (cost < 2) ? 2 : cost;
  };
};

export class PredictionPlace implements AbstractPlace {
  constructor(
    public mainText = '',
    public secondaryText = '',
  ) { }
  getViewText(): string {
    return this.mainText + (this.secondaryText ? `,  ${this.secondaryText}` : '');
  };
  getPosition(): string {
    return this.mainText + (this.secondaryText ? `,  ${this.secondaryText}` : '');
  };
};

export class Location implements AbstractPlace {
  constructor(
    public location = new LatLng(53.9, 27.56),
    public mainText = 'Current location',
  ) { }
  getViewText() {
    return 'Current location';
  }
  getPosition(): google.maps.LatLng {
    return new google.maps.LatLng(this.location.lat, this.location.lng);
  }

  static fromPosition(pos: GeolocationPosition) {
    const latLng = new LatLng(pos.coords.latitude, pos.coords.longitude);
    return new Location(latLng);
  }
}

export interface durationAndDistance {
  duration: {
    value: number,
    text: string,
  },
  distance: {
    value: number,
    text: string,
  },
};

export class LatLng {
  constructor(
    public lat: number,
    public lng: number,
    public noWrap?: boolean) {
  };
};


export interface Place {
  location?: LatLng;
  placeId?: string;
  query?: string;
};
