import { Component, Vue } from "vue-property-decorator";

import './app.component.scss';

@Component({
  template: `
  <div id="app">
    <div id="nav">
      <router-link class="router-link" to="/request-taxi">Taxi</router-link> 
      <router-link class="router-link" to="/about">About</router-link>
    </div>
    <router-view/>
  </div>
  `
})
export default class AppComponent extends Vue {
  mounted() {
    this.$router.push('/request-taxi');
  };
};
