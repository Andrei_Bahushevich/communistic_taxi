import { debounce } from "lodash";

export default function Debounce(time: number) {
    return function (target: Object, propertyKey: string, descriptor: PropertyDescriptor) {
        descriptor.value = debounce(descriptor.value, time);
    }
}
