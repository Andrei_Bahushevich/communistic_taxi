import Vue from 'vue';
import RequestTaxi from '@/components/request-taxi/request-taxi.component';
import About from './views/About.vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/request-taxi',
      name: 'requestTaxi',
      component: RequestTaxi,
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
  ],
});
