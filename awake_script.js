const https = require('https');

get();

function get() {
  https.get('https://kommuna-taxi.herokuapp.com', () => console.log(new Date() + ' requested!'));
  setTimeout(get, 10*60*1000);
}
