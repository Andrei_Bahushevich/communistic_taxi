const express = require('express');
const history = require('connect-history-api-fallback');

const app = express();

app.use(express.static('dist'));
app.use(history({
  verbose: true,
}));
app.use(express.static('dist'));


const port = process.env.PORT || "4300";
const ip = process.env.IP || '0.0.0.0'

app.listen(port, ip);
console.log(`server started on ${ip}:${port}`);
